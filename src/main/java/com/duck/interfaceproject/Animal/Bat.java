/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.interfaceproject.Animal;

/**
 *
 * @author Administrator
 */
public class Bat extends Poultry{
    private String nickname;
    public Bat(String nickname) {
        super();
    }

    @Override
    public void eat() {
         System.out.println("Bat : "+nickname+" eat");
    }

    @Override
    public void speak() {
        System.out.println("Bat : "+nickname+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat : "+nickname+" sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bat : "+nickname+" fly");
    }
    
}
