/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.interfaceproject.Animal;

/**
 *
 * @author Administrator
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Bat");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog();
        
        Flyable[] flyables = {bat,plane};
        for(Flyable f : flyables){
        if(f instanceof Plane){
           Plane p = (Plane)f;
           p.startEngine();
           p.run();
        }    
        f.fly();
        }
        Runable[] runables = {dog,plane};
        for(Runable r : runables){
           r.run(); 
        }
    }
}
