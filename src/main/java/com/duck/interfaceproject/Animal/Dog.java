/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.interfaceproject.Animal;

/**
 *
 * @author Administrator
 */
public class Dog extends LandAnimal {

    public Dog(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }

    @Override
    public void eat() {
        System.out.println("Dog : eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog : run");
    }
    
}
